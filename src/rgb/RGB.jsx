import { useState } from 'react';
import './rgb.css';


function RGB() {

    const [r, setR] = useState(0);
    const [g, setG] = useState(0);
    const [b, setB] = useState(0);

    return (
        <div className="rgb">
            <input type="range" value={r} min="0" max="255" onChange={(e) => setR(e.target.value)} />
            <input type="range" value={g} min="0" max="255" onChange={(e) => setG(e.target.value)} />
            <input type="range" value={b} min="0" max="255" onChange={(e) => setB(e.target.value)} />
            <h2>{`rgb(${r}, ${g}, ${b})`}</h2>
            <div className="mostra" style={{ backgroundColor: `rgb(${r}, ${g}, ${b})` }}></div>
        </div>
    )
}



export default RGB;