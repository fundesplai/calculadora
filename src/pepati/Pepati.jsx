
import { useState, useEffect } from 'react';
import './pepati.css';

// import Selector from './Selector';
// import Partida from './Partida';

import Tisora from './img/tisora.jpg';
import Paper from './img/paper.jpg';
import Pedra from './img/pedra.jpg';


const imatges = [Pedra, Paper, Tisora];


function randomIntFromInterval(min, max) { // min and max included 
    return Math.floor(Math.random() * (max - min + 1) + min)
}


function Selector(props) {

    return (
        <div className="selector">
            <img src={Pedra} alt="" onClick={() => props.tria(1)} />
            <img src={Paper} alt="" onClick={() => props.tria(2)} />
            <img src={Tisora} alt="" onClick={() => props.tria(3)} />
        </div>
    )
}

function Partida({ jugada }) {
    let resposta;
    let winners = ['13', '21', '32']; // pedra-tisora, paper-pedra, tisora-paper
    let losers = ['31', '12', '23']; // tisora-pedra, pedra-paper, paper-tisora

    if (jugada === 0) {
        return (
            <div className="partida">
                <h1>Jugar!</h1>
            </div>
        );

    } else {
        resposta = randomIntFromInterval(1, 3);
        let resultat = 'empat';
        if (winners.includes('' + jugada + resposta)) resultat = 'guanyes';
        if (losers.includes('' + jugada + resposta)) resultat = 'perds';

        return (
            <>
                <div className="partida">
                    <img src={imatges[jugada - 1]} />
                    <img src={imatges[resposta - 1]} />
                </div>
                <div className="resultat">

                    <p>{resultat}</p>
                </div>
            </>
        );
    }
}


function Pepati() {

    const [jugada, setJugada] = useState(0);

    return (
        <div className="pepati">
            <Selector tria={setJugada} />
            <Partida jugada={jugada} />
        </div>
    )
}


export default Pepati;