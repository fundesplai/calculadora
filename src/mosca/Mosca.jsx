import { useState } from 'react';
import './mosca.css';


function Mosca() {

    const [pos, setPos] = useState(0);


    return (
        <>

            <input type="range" value={pos} min="0" max="460" onChange={(e) => setPos(e.target.value)} />
            <div className="contenidor">
                <div className="mosca" style={{ left: pos + 'px' }}>

                </div>
            </div>

        </>
    )
}



export default Mosca;