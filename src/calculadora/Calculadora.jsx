import { useState, useEffect } from 'react';

import Pantalla from './Pantalla';
import Teclado from './Teclado';

import './calculadora.css';



export const Calculadora = () => {

    const [mostrar, setMostrar] = useState('0');
    const [error, setError] = useState(false);

    function pulsa(tecla) {

        if (tecla === 'C' || tecla === 'c') {
            // boton C o teclas c/C
            setMostrar('0');
        } else if (tecla === 'B') {
            //backspace o delete, borramos último caracter
            //no hay botón equivalente, sólo sirve teclado
            if (mostrar.length > 1) {
                setMostrar(mostrar.slice(0, -1));
            } else {
                setMostrar('0');
            }
        } else if (tecla === '=') {
            // = calculamos
            let resultado;
            // eval(x) evalúa el string 'x' en busca de una expresión javascript válida
            // tal como: "1+3*4/2"
            // pero expresion podría ser errónea, ej: "1234+3**"
            // en este caso saltará a "catch"
            try {
                resultado = eval(mostrar);
                setMostrar(resultado + ''); //interesa que sea string...
            } catch {
                //activamos error
                setError(true);
            }
        } else {
            setMostrar(mostrar === '0' ? tecla : mostrar + tecla);
        }


    }

    useEffect(() => {
        //si estamos en error, evaluamos posible desactivacion
        if (error) {
            try {
                eval(mostrar);
                setError(false);
            } catch {
                //si sigue fallando eval no hacemos nada
            }
        }

        function pulsaTeclado(e) {
            let laTecla = e.key; // key es el caracter en string
            let code = e.keyCode || e.charCode; // esc, intro... van por código
            if ("1234567890Cc=*+-/.".includes(laTecla)) {
                pulsa(laTecla);
            } else if (code == 13) {
                // enter
                pulsa('=');
            } else if (code == 27) {
                // esc
                pulsa('C');
            } else if (code == 8 || code == 46) {
                // backspace o delete
                pulsa('B');
            } else {
                //tecla no aceptada...
            }
        }

        document.addEventListener('keydown', pulsaTeclado);
        //es necesario limpiar evento cuando se vuelve a cargar useefect
        return function cleanup() {
            document.removeEventListener('keydown', pulsaTeclado);
        }
    }, [mostrar]); // redefinir evento cada vez que cambia el display

    const teclas = [
        '1', '2', '3', '/', '4', '5', '6', '*', '7', '8', '9', '-', 'C', '0', '=', '+'
    ];

    return (
        <div className="calculadora">
            <Pantalla mostrar={mostrar} error={error} />
            <Teclado teclas={teclas} pulsa={pulsa} />
        </div>
    )
}

export default Calculadora;