


function Tecla(props){
    const csstecla = props.valor==='C' ? 'tecla rojo' : 'tecla';
    return <div className={csstecla} onClick={()=>props.onClick(props.valor)}>{props.valor}</div>
}



function Teclado(props){

    return (
        <div className="teclado">
            {props.teclas.map((e,idx)=><Tecla key={idx} valor={e} onClick={()=>props.pulsa(e)} />)}
        </div>
    )

}

export default Teclado;
